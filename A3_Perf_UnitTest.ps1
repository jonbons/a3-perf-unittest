$gameArgs = "-noLauncher -par=a3-perf-test.txt -autotest=A3_Perf_UnitTest.cfg"

$gameDir = "C:\Program Files (x86)\Steam\steamapps\common\Arma 3" #change to your A3 directory
$logDir = $env:LOCALAPPDATA + "\Arma 3" #change JonBons to your windows username

function testAllMods
{    
    Write-Host ("Testing modded content")
    #start game and wait for finish
    Start-Process -FilePath ($gameDir + "\arma3_x64.exe") -ArgumentList $gameArgs -NoNewWindow -Wait

    Start-Sleep -s 1

    #get log file
    $latestFile = gci $logDir | sort LastWriteTime | select -last 1
    $logContents = Get-Content ($logDir + "\" + $latestFile)

    $nl = [Environment]::NewLine

    #OUTPUT WEAPON RESULTS TO CSV

    $weaponLines = ([regex]"<WEAPON_FPS class='(.*?)'>(.*?)</WEAPON_FPS>").Matches($logContents)

    $weaponCSV = "Prefix,Classname,AvgFPS,Error" + $nl

    foreach ($match in $weaponLines) {
        $weaponCSV = $weaponCSV + ($match.Groups[1].Value.Split("_")[0] + "," + $match.Groups[1].Value + "," + $match.Groups[2].Value) + $nl
    }

    $weaponCSV | Out-File ".\a3_perf_weapons.csv"

    #OUTPUT UNIFORM RESULTS TO CSV

    $uniformLines = ([regex]"<UNIFORM_FPS class='(.*?)'>(.*?)</UNIFORM_FPS>").Matches($logContents)

    $uniformCSV = "Prefix,Classname,AvgFPS,Error" + $nl

    foreach ($match in $uniformLines) {
        $uniformCSV = $uniformCSV + ($match.Groups[1].Value.Split("_")[0] + "," + $match.Groups[1].Value + "," + $match.Groups[2].Value) + $nl
    }

    $uniformCSV | Out-File ".\a3_perf_uniforms.csv"

    #OUTPUT VEST RESULTS TO CSV

    $vestLines = ([regex]"<VEST_FPS class='(.*?)'>(.*?)</VEST_FPS>").Matches($logContents)

    $vestCSV = "Prefix,Classname,AvgFPS,Error" + $nl

    foreach ($match in $vestLines) {
        $vestCSV = $vestCSV + ($match.Groups[1].Value.Split("_")[0] + "," + $match.Groups[1].Value + "," + $match.Groups[2].Value) + $nl
    }

    $vestCSV | Out-File ".\a3_perf_vests.csv"


    #$logValue = [regex]::match($latestFile, '<AVERAGE FPS>(.*?)</AVERAGE FPS>').Groups[1].Value

    #$pboFps += ,@('ace_all', $logValue)
    #Write-Host "Test ran and got $logValue FPS"
    

}

testAllMods #test all mods via adding more one at a time

#disableAllMods
#Write-Host "Disabled mod pbos"
#testEachMod #test only one mod pbo enabled at a time
