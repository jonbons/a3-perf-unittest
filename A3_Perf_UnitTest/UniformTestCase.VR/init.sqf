enableSaving [false,false];

jb_benchmark_sum = {
    private _array = _this;
    private _sum = 0;

    {
        _sum = _sum + _x;
    } forEach _array;
    _sum
};

jb_benchmark_standardDeviation = {
    params ["_array", ["_ddof", 0, [0]]];
    private _N = (count _array) - _ddof;
    if (_N <= 0) exitWith {0};
    private _mean = _array call BIS_fnc_arithmeticMean;
    private _array = _array apply {(_x - _mean)^2};
    private _sum = _array call jb_benchmark_sum;
    sqrt (_sum / _N)
};

jb_benchmark_averaged_fps = {
    params [["_cycles", 10]];
    _fps = diag_fps;
    _fpsArray = [_fps];
    for "_iFPS" from 0 to (4*_cycles) do {
        _frameTimeDelay = (1/(_fps)) * 18;
        sleep _frameTimeDelay;
        _fps = diag_fps;
        _fpsArray pushBack _fps;
    };

    [_fpsArray call BIS_fnc_arithmeticMean,  [_fpsArray, 0] call jb_benchmark_standardDeviation]
};

diag_log "<UNIFORM_RESULTS>";

_classnameFPS = [];
_aiUnits = allUnits;
_assaultRifles = "(getNumber (_x >> 'scope')) == 2 && {'Uniform' in ( [configName _x] call BIS_fnc_itemType )}" configClasses (configFile >> "CfgWeapons");

sleep 0.5;

{
	_class = configName _x;
	diag_log format ["TESTING: %1", _class];
} forEach _assaultRifles;

{
	if (!isPlayer _x) then {
		_x enableSimulation false;
	};
} forEach _aiUnits;

removeAllWeapons player;
sleep 0.1;

// START BENCHMARK
benchmarkRunning = true;
startTime = diag_tickTime;
startFrame = diag_frameno;

titleText ["Benchmarking started", "PLAIN DOWN", 0.3];

sleep 5;

_lastClass = "";

{
	_class = configName _x;

	_result = call jb_benchmark_averaged_fps;

	if (_lastClass != "") then {
		diag_log format["<UNIFORM_FPS class='%3'>%1,%2</UNIFORM_FPS>", _result select 0, _result select 1, _lastClass];
	};
	titleText [format["AVERAGE FPS %1:%2", _result select 0, _lastClass], "PLAIN DOWN", 0.3];

	_classnameFPS pushBack [_lastClass, _result];

	{
		if (!isPlayer _x) then {
			removeUniform _x;
			_x forceAddUniform _class;
		};
	} forEach _aiUnits;

	sleep 3;

	_lastClass = _class;

} forEach _assaultRifles;

diag_log "<UNIFORM_RESULTS>";

// END
benchmarkRunning = false;
endMission "end1";
